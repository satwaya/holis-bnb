import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import {
  CreateLocationDto,
  GetLocationByIdDto,
  UpdateLocationDto,
} from './Location.dto';
import { LocationService } from './Location.service';

@Controller('locations')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  /** List all locations in database with this endpoint */
  @Get()
  async getLocations() {
    return await this.locationService.getLocations();
  }

  /** Get a specific location in database */
  @Get(':id')
  async getLocationById(@Param() params: GetLocationByIdDto) {
    const location = await this.locationService.getLocationById(params.id);
    if (!location) {
      return new NotFoundException('Location not found');
    } else {
      return location;
    }
  }
  /** Create a new location in database */
  @Post()
  async createLocation(@Body() body: CreateLocationDto) {
    return await this.locationService.createLocation(body);
  }

  /** Update a specific location in database */
  @Put(':id')
  async updateLocationById(
    @Param() params: GetLocationByIdDto,
    @Body() body: UpdateLocationDto,
  ) {
    //
    const location = await this.locationService.getLocationById(params.id);
    if (!location) {
      return new NotFoundException('Location not found');
    } else {
      return await this.locationService.updateLocationPrice(
        location,
        body.price,
      );
    }
  }

  /** Delete a specific location in database */
  @Delete(':id')
  async deleteLocationById(@Param() params: GetLocationByIdDto) {
    return await this.locationService.deleteLocationById(params.id);
  }
}
