import { ApiBody, ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsNumberString,
  IsPositive,
  IsString,
  Max,
  Min,
} from 'class-validator';

/**
 * This dto is used to control user inputs and make sure it is valid data to create locations.
 * If the input provided to the endpoint does not match the rules defined by decorators here,
 * the endpoint will immediately return an error.
 * More info here: https://docs.nestjs.com/techniques/validation
 */

export class CreateLocationDto {
  @ApiProperty({ type: String })
  @IsString()
  title: string;

  @ApiProperty({ type: String })
  @IsString()
  description: string;

  @ApiProperty({ type: String })
  @IsString()
  location: string;

  @ApiProperty({ type: String })
  @IsString()
  picture: string;

  @ApiProperty({ type: String })
  @IsString()
  categoryName: string;

  @ApiProperty({ type: Number })
  @IsNumberString()
  @IsPositive()
  @Min(1)
  numberOfRooms: number;

  @ApiProperty({ type: Number })
  @IsNumberString()
  @IsPositive()
  price: number;

  @ApiProperty({ type: Number })
  @IsNumberString()
  @IsPositive()
  @Min(0)
  @Max(5)
  stars: number;
}
export class GetLocationByIdDto {
  @ApiProperty({ type: Number })
  @IsNumberString()
  id: number;
}

export class UpdateLocationDto {
  @ApiProperty({ type: Number })
  @IsNumberString()
  price: number;
}
