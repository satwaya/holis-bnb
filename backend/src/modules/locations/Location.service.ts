import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InsertResult, Repository } from 'typeorm';
import { CategoryService } from '../categories/Category.service';
import { CreateLocationDto } from './Location.dto';
import { Location } from './Location.entity';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location)
    private readonly locationRepository: Repository<Location>,
    private readonly categoryService: CategoryService,
  ) {}

  async getLocations() {
    return await this.locationRepository.find();
  }

  async getLocationById(id: number) {
    return await this.locationRepository.findOne(id, {
      relations: ['category'],
    });
  }

  async createLocation(info: CreateLocationDto) {
    // Search for an existing category
    const lookForExistingCategory =
      await this.categoryService.getCategoryByName(info.categoryName); // ESLint Prettier doing weird things :)

    if (lookForExistingCategory.length > 0) {
      const category = lookForExistingCategory[0];
      return await this.locationRepository.insert({
        ...info,
        categoryId: category.id,
        category,
      });
    }

    // Create it and then do the insertion
    const newlyCreatedCategory = (await this.categoryService.createCategory({
      name: info.categoryName,
      description: 'New Description',
    })) as InsertResult;

    // Code duplication as we love :)
    return this.locationRepository.insert({
      ...info,
      categoryId: newlyCreatedCategory.raw[0].id,
    });
  }

  async updateLocationPrice(location: Location, price: number) {
    location.price = price;
    return await this.locationRepository.save(location);
  }

  async deleteLocationById(id: number) {
    return await this.locationRepository.delete(id);
  }
}
