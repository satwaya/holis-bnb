import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateCategoryDto, GetCategoryByIdDto } from './Category.dto';
import { CategoryService } from './Category.service';

@Controller('categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  /** List all categories in database with this endpoint */
  @Get()
  async getCategories() {
    return await this.categoryService.getCategories();
  }

  /** Get a specific category in database */
  @Get(':id')
  async getCategoryById(@Param() params: GetCategoryByIdDto) {
    const category = await this.categoryService.getCategoryById(params.id);
    if (!category) {
      return new NotFoundException('Category not found');
    } else {
      return category;
    }
  }
  /** Create a new category in database */
  @Post()
  async createCategory(@Body() body: CreateCategoryDto) {
    return await this.categoryService.createCategory(body);
  }
}
