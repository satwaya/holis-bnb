import { DefaultValuePipe } from '@nestjs/common';
import { ApiBody, ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class CreateCategoryDto {
  @ApiProperty({ type: String })
  @IsString()
  name: string;

  @ApiProperty({ type: String })
  @IsString()
  @IsOptional()
  description = 'New Description';
}

export class GetCategoryByIdDto {
  @ApiProperty({ type: Number })
  @IsNumberString()
  id: number;
}
