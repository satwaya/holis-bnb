import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './Category.dto';
import { Category } from './Category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
  ) {}

  async getCategories() {
    return await this.categoryRepository.find({
      relations: ['locations'],
    });
  }

  async getCategoryById(id: number) {
    return await this.categoryRepository.findOne(id, {
      relations: ['locations'],
    });
  }

  async getCategoryByName(name: string) {
    return await this.categoryRepository.find({ name });
  }

  async createCategory(info: CreateCategoryDto) {
    const lookForExistingCategory = await this.getCategoryByName(info.name);
    // Avoid duplicates by looking for existing category with provided name
    if (lookForExistingCategory.length === 0) {
      return await this.categoryRepository.insert({ ...info });
    } else {
      return new ConflictException(
        lookForExistingCategory[0],
        'Category name already taken',
      );
    }
  }
}
