export {};

const API_BASE_URL = 'http://localhost:8000';

export const getLocationsPerCategory = async () => {
  const req = await fetch(`${API_BASE_URL}/categories`);
  const res = await req.json();

  return res;
};

export const getLocationById = async (locationId: string) => {
  const req = await fetch(`${API_BASE_URL}/locations/${locationId}`);
  const res = await req.json();

  return res;
};

export const deleteLocation = async (locationId: string) => {
  const req = await fetch(`${API_BASE_URL}/locations/${locationId}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  const res = await req.json();

  return res;
};

export const updateLocation = async (locationId: string, price: number) => {
  const req = await fetch(`${API_BASE_URL}/locations/${locationId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ price })
  });
  const res = await req.json();

  return res;
};
