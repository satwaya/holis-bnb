import React from 'react';
import { Link } from 'react-router-dom';
import { Location } from '../../data-types';
import './Card.css';

type CardProps = {
  room: Location;
};

const Card: React.FC<CardProps> = ({ room }) => {
  return (
    <Link to={`/room/${room.id}`}>
      <div className="card">
        <img
          src={room.picture}
          alt={`Découvrez ${room.title} en images`}
          className="card--picture"
        />
        <h4>{room.title}</h4>
        <p>€ {Intl.NumberFormat('en-US').format(room.price)} / night</p>
      </div>
    </Link>
  );
};

export default Card;
