import { useEffect, useState } from 'react';
import { Category as CategoryType, Location } from '../../data-types';
import Card from '../Card/Card';
import './Category.css';

export default function Category({ category }: { category: CategoryType }) {
  // Don't list categories with no rooms in it
  if (category.locations.length === 0) return null;

  const [rooms, setRooms] = useState<{ [key: string]: Location[] }>({});

  // Subfilter category per number of rooms
  useEffect(() => {
    const sortedRooms = {} as { [key: string]: Location[] };

    category.locations
      .sort((l1, l2) => l1.numberOfRooms - l2.numberOfRooms) // Sort by number of rooms ASC
      .forEach((location) => {
        const numberOfRoomsKey = `${location.numberOfRooms}-rooms`;
        if (numberOfRoomsKey in sortedRooms) {
          sortedRooms[numberOfRoomsKey].push(location);
        } else {
          sortedRooms[numberOfRoomsKey] = [location];
        }
      });

    setRooms(sortedRooms);
  }, [category]);

  return (
    <section className="category-container">
      <h2 className="category-container--title">{category.name}</h2>
      <hr />
      {Object.entries(rooms).map(([numberOfRoomsSlug, locations]) => (
        <div className="room-container" key={numberOfRoomsSlug}>
          <h3 className="category-container--title">{numberOfRoomsSlug.split('-').join(' ')}</h3>
          <div className="roomcard-container">
            {locations.map((location) => (
              <Card room={location} key={location.id} />
            ))}
          </div>
        </div>
      ))}
    </section>
  );
}
