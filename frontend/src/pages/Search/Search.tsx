import React, { useEffect, useState } from 'react';
import { useRecoilValue } from 'recoil';
import { searchState } from '../../atoms';
import Category from '../../components/Category/Category';
import { Category as CategoryType } from '../../data-types';
import { getLocationsPerCategory } from '../../services/HolisbnbAPI.service';
import './Search.css';

type SearchPageProps = {};

const SearchPage: React.FC<SearchPageProps> = (props) => {
  const [locations, setLocations] = useState<CategoryType[]>([]);
  const [filteredLocations, setFilteredLocations] = useState<CategoryType[]>(locations);
  const search = useRecoilValue(searchState);

  // Initial fetch
  useEffect(() => {
    getLocationsPerCategory().then((res) => setLocations(res));
  }, [props]);

  // Sort locations by number of rooms + connected to the search in the header
  useEffect(() => {
    setFilteredLocations(
      locations.map((category) => ({
        ...category,
        locations: category.locations.filter((location) =>
          location.title.toLowerCase().includes(search.toLowerCase())
        )
      }))
    );
  }, [locations, search]);

  return (
    <div className="search">
      {filteredLocations.map((category) => (
        <Category category={category} key={category.id} />
      ))}
      {/* List of sorted locations card */}
    </div>
  );
};

export default SearchPage;
