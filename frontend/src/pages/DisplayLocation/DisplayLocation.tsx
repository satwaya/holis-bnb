import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import {
  deleteLocation,
  getLocationById,
  updateLocation
} from '../../services/HolisbnbAPI.service';
import { Location } from '../../data-types';
import './DisplayLocation.css';
import { toast } from 'react-toastify';

type DisplayLocationPageProps = {};

const DisplayLocationPage: React.FC<DisplayLocationPageProps> = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [room, setRoom] = useState<Location>();

  // Fetch data
  useEffect(() => {
    getLocationById(id as string)
      .then((res) => {
        if (res.statusCode !== undefined) {
          navigate('/');
        } else {
          setRoom(res);
        }
      })
      .catch(() => navigate('/'));
  }, [id]);

  // Handle update
  const onClickUpdate = useCallback(
    async (event: any) => {
      event.preventDefault();
      try {
        await updateLocation(id as string, (room as Location).price);
        toast('Prix correctement mis à jour');
      } catch (error) {
        console.log(error);
        toast('Une erreur interne est survenue.');
      }
    },
    [room]
  );

  // Handle delete
  const onClickDelete = useCallback(async (event: any) => {
    //
    event.preventDefault();
    try {
      await deleteLocation(id as string);
      toast('Suppression réussie');
      navigate('/');
    } catch (error) {
      console.log(error);
      toast('Une erreur interne est survenue.');
    }
  }, []);

  // Handle price change
  const onPriceChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setRoom({
        ...(room as Location),
        price: event.target.value as unknown as number
      });
    },
    [room]
  );

  return (
    <div className="display-location">
      <img
        src={room?.picture}
        alt="Image de couverture du bien"
        className="display-location--cover"
      />

      <div className="display-location--container">
        <div className="display-location__content">
          <div className="display-location__content--headline">
            <h1>{room?.title}</h1>
            <h2>
              <strong>€ {Intl.NumberFormat('en-US').format(room?.price ?? 0)}</strong> per night
            </h2>
          </div>
          <p>
            <span className="display-location__content--category-name">{room?.category?.name}</span>{' '}
            • {room?.numberOfRooms} rooms
          </p>
          <p>{room?.description}</p>
        </div>

        <div className="display-location__edit-container">
          <form className="display-location__edit">
            <label>Modify price</label>
            <input
              type="number"
              value={room?.price}
              className="display-location__edit--price-input"
              onChange={onPriceChange}
            />
            <div className="display-location__edit--cta-container">
              <input
                type="submit"
                value="Delete"
                className="display-location__edit--delete-btn"
                onClick={onClickDelete}
              />
              <input
                type="submit"
                value="Confirm"
                className="display-location__edit--confirm-btn"
                onClick={onClickUpdate}
              />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default DisplayLocationPage;
